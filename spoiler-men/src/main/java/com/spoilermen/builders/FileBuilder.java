package com.spoilermen.builders;

import com.spoilermen.business.SpoilerBusiness;
import com.spoilermen.models.File;
import com.spoilermen.utils.FileUtil;

public class FileBuilder {
	
	private SpoilerBusiness spoilerBusiness = new SpoilerBusiness();
	public File file;
	
	public FileBuilder(File file) {
		this.file = file;
	}
	
	public static FileBuilder withNew() {
		return new FileBuilder(new File());
	}

	public FileBuilder path(String path) {
		this.file.setPath(path);
		return this;
	}
	
	public File build() {
		this.file.setContent(FileUtil.read(this.file.getPath()));
		this.file.setSpoilers(spoilerBusiness.generateSpoilers(this.file));
		return this.file;
	}
	
}
