package com.spoilermen.models;

public class Spoiler {

	String name;
	
	public Spoiler() {
	}
	
	public Spoiler(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
}
