package com.spoilermen.models;

import java.util.List;

public class File {

	String path;
	List<String> content;
	List<Spoiler> spoilers;
	
	public File() {
	}
	
	public File(String path) {
		this.path = path;
	}
	
	public String getPath() {
		return path;
	}
	
	public void setPath(String path) {
		this.path = path;
	}
	
	public List<String> getContent() {
		return content;
	}
	
	public void setContent(List<String> content) {
		this.content = content;
	}

	public void setSpoilers(List<Spoiler> spoilers) {
		this.spoilers = spoilers;
	}
	
	public List<Spoiler> getSpoilers() {
		return spoilers;
	}
	
}
