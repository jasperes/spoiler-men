package com.spoilermen.factorys;

import java.util.List;

import com.spoilermen.business.GetSpoilersOffline;
import com.spoilermen.business.GetSpoilersOnline;
import com.spoilermen.interfaces.GetSpoilers;
import com.spoilermen.models.Spoiler;

public class GetSpoilersFactory implements GetSpoilers {

	boolean online;
	
	public GetSpoilersFactory(boolean online) {
		this.online = online;
	}
	
	public List<Spoiler> read(String filePath) {
		GetSpoilers getSpoilers = this.online ? new GetSpoilersOnline() : new GetSpoilersOffline();
		return getSpoilers.read(filePath);
	}
	
}
