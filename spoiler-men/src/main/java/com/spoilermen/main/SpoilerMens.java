package com.spoilermen.main;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.spoilermen.factorys.GetSpoilersFactory;
import com.spoilermen.models.Spoiler;

public class SpoilerMens {
	
	public static String getRandom(String filePath) {
		return getRandom("", false, filePath);
	}
	
	public static String getRandom(String regex, boolean online, String filePath) {
		List<Spoiler> spoilers = filtrarSpoilers(regex, new GetSpoilersFactory(online).read(filePath));
		return randomSpoiler(spoilers);
	}
	
	private static List<Spoiler> filtrarSpoilers(String regex, List<Spoiler> spoilers) {
		List<Spoiler> filtredSpoilers = new ArrayList<Spoiler>();
		
		if (listIsNotEmpty(spoilers)) {
			for (Spoiler spoiler : spoilers) {
				if (spoilerHaveName(spoiler) && isRegex(regex, spoiler.getName())) {
					filtredSpoilers.add(spoiler);
				}
			}
		}
		
		return filtredSpoilers;
	}
	
	private static boolean listIsNotEmpty(List<Spoiler> spoilers) {
		return spoilers != null && spoilers.size() > 0;
	}
	
	private static boolean spoilerHaveName(Spoiler spoiler) {
		return spoiler != null && spoiler.getName() != null;
	}

	private static boolean isRegex(String regex, String name) {
		return name.matches(regex);
	}
	
	private static String randomSpoiler(List<Spoiler> spoilers) {
		return listIsNotEmpty(spoilers) ? spoilers.get(new Random().nextInt(spoilers.size())).getName() : null;
	}
}