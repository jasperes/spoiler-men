package com.spoilermen.utils;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FileUtil {

	public static List<String> read(String filePath) {
		return getLines(getFile(filePath));
	}
	
	private static Scanner getFile(String filePath) {
		Scanner file = null;
		
		try {
			file = new Scanner(new FileReader(filePath));
			file.next();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return file;
	}
	
	private static List<String> getLines(Scanner file) {
		List<String> lines = new ArrayList<String>();
		
		while (file != null && file.hasNext()) {
			lines.add(file.next());
		}
		
		return lines;
	}
	
}
