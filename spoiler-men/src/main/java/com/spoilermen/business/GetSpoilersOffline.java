package com.spoilermen.business;

import java.util.List;

import com.spoilermen.builders.FileBuilder;
import com.spoilermen.interfaces.GetSpoilers;
import com.spoilermen.models.Spoiler;

public class GetSpoilersOffline implements GetSpoilers {

	@Override
	public List<Spoiler> read(String filePath) {
		return FileBuilder.withNew().path(filePath).build().getSpoilers();
	}

}
