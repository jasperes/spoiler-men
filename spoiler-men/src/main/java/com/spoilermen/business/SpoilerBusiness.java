package com.spoilermen.business;

import java.util.ArrayList;
import java.util.List;

import com.spoilermen.models.File;
import com.spoilermen.models.Spoiler;

public class SpoilerBusiness {

	public List<Spoiler> generateSpoilers(File file) {
		List<Spoiler> generatedSpoilers = new ArrayList<Spoiler>();
		
		if (fileHaveContent(file)) {
			for (String spoiler : file.getContent()) {
				generatedSpoilers.add(new Spoiler(spoiler));
			}
		}
		
		return generatedSpoilers;
	}
	
	private boolean fileHaveContent(File file) {
		return file != null && file.getContent() != null;
	}
	
}
