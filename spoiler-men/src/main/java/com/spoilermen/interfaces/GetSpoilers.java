package com.spoilermen.interfaces;

import java.util.List;

import com.spoilermen.models.Spoiler;

public interface GetSpoilers {

	List<Spoiler> read(String filePath);
}
